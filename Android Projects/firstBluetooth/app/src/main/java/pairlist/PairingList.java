package pairlist;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import weissen.firstbluetooth.R;

/**
 * Created by Weiss on 17.08.2016.
 */
public class PairingList extends Activity {
    ListView lview;
    String[] paires;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pairinglist_layout);
        lview = (ListView) findViewById(R.id.deviceView);
        Bundle bn = getIntent().getExtras();
        paires = bn.getStringArray("paires");

        if (paires != null) {
            adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, paires);
        }
        lview.setAdapter(adapter);
    }
}
