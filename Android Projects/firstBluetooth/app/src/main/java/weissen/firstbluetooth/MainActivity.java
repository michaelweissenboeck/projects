package weissen.firstbluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Set;

public class MainActivity extends AppCompatActivity {
    Button bnTurnOn; //Button for turning on Bluetooth
    //Button bnPairList; //Button for pairinglist Bluetooth

    BluetoothAdapter b_adapter;
    int BLUETOOTH_REQUEST = 1;
    Set<BluetoothDevice> paired_devices;
    String plist[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //FOR TURNING ON BLUETOOTH
        bnTurnOn = (Button)findViewById(R.id.turnOnBtn);

        bnTurnOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                b_adapter = BluetoothAdapter.getDefaultAdapter();

                if (b_adapter == null) {
                    Toast.makeText(getBaseContext(), "No Bluetooth Adapter found", Toast.LENGTH_LONG).show();
                }
                else {
                    if (b_adapter.isEnabled())
                        {
                            Toast.makeText(getBaseContext(), "Bluetoothadapter bereits aktiviert", Toast.LENGTH_LONG).show();
                        }
                        else if (!b_adapter.isEnabled())
                            {
                                Intent i = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                                startActivityForResult(i, BLUETOOTH_REQUEST);
                            }
                }
            }
        });

/*
        //FOR PAIRING LIST BLUETOOTH
        bnPairList=(Button)findViewById((R.id.listBtnDev));
        bnPairList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {

            }
        });
*/
    }

    //FOR TURNING ON BLUETOOTH
    public void onActivityResult(int request_code, int result_code, Intent data) {
        if (request_code==BLUETOOTH_REQUEST) {
            if (result_code==RESULT_OK) {
                Toast.makeText(getBaseContext(), "AUFGEDREHT YOLO", Toast.LENGTH_LONG).show();
                paired_devices = b_adapter.getBondedDevices();

                int count = paired_devices.size(); //get number of devices
                plist = new String[count];

                int i = 0;
                for (BluetoothDevice device: paired_devices) {
                    plist[i] = device.getName().toString();
                    i++;
                }

                Bundle bn = new Bundle();
                bn.putStringArray("paires", plist);
                Intent in = new Intent("pair_filter");

                in.putExtras(bn);
                startActivity(in);
            }
            if (result_code==RESULT_CANCELED) {
                Toast.makeText(getBaseContext(), "BLUETOOTH TURN ON FAILED", Toast.LENGTH_LONG).show();
            }
        }
    }
}
